TradeMons:
; givemonster, getmonster, textstring, nickname (11 bytes), 14 bytes total
	db RATICATE,  POLIWHIRL, 0,"POLI@@@@@@@"
	db PSYDUCK,      SLOWPOKE,  0,"PRICYTAIL@@"
	db BUTTERFREE,BEEDRILL, 2,"CHIKUCHIKU@"
	db HYPNO,    HAUNTER,     0,"BLACKFOG@@@"
	db LICKITUNG,   CLEFAIRY,2,"CLIFF@@@@@@"
	db PARASECT,   PERSIAN,0,"PERSHIE@@@@"
	db PIDGEY, SPEAROW,     1,"BIRDOFPREY@"
	db BUTTERFREE,    BEEDRILL,1,"SPEARHEAD@@"
	db ELECTRODE,   MAGNETON,  2,"CLANG@@@@@@"
	db NIDORAN_M, NIDORAN_F,2,"SPOT@@@@@@@"
